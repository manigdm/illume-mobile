var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
import { Component, ViewChild } from '@angular/core';
import { Storage } from '@ionic/storage';
import { Platform, MenuController, Nav, App, Events } from 'ionic-angular';
import { StatusBar } from '@ionic-native/status-bar';
import { SplashScreen } from '@ionic-native/splash-screen';
// import { SocialSharing } from '@ionic-native/social-sharing';
import { HomePage } from '../pages/home/home';
import { LoginPage } from '../pages/login/login';
import { RegisterPage } from '../pages/register/register';
import { AboutPage } from '../pages/about/about';
import { ContactPage } from '../pages/contact-us/contact-us';
// import { RecentViewPage } from '../pages/recent-view/recent-view';
import { WishListPage } from '../pages/wish-list/wish-list';
import { MyCartsPage } from '../pages/my-carts/my-carts';
import { AccountPage } from '../pages/account/account';
import { HelpPage } from '../pages/help/help';
import { MyOrdersPage } from '../pages/my-orders/my-orders';
// import { AuthPage } from '../pages/auth/auth';
// import { ProductListPage } from '../pages/product-list/product-list';
var MyApp = /** @class */ (function () {
    function MyApp(platform, statusBar, splashScreen, menu, app, events, storage) {
        var _this = this;
        this.menu = menu;
        this.app = app;
        this.events = events;
        this.storage = storage;
        // make LearnFeedPage the root (or first) page
        this.rootPage = MyOrdersPage;
        platform.ready().then(function () {
            // Okay, so the platform is ready and our plugins are available.
            // Here you can do any higher level native things you might need.
            statusBar.styleDefault();
            splashScreen.hide();
        });
        this.pages = [
            {
                title: 'Home',
                component: HomePage
            },
            // {
            //   title: 'Recently Viewed Item',
            //   component: RecentViewPage
            // },
            {
                title: 'My Cart',
                component: MyCartsPage
            },
            {
                title: 'My Wish List',
                component: WishListPage
            },
            {
                title: 'My Account',
                component: AccountPage
            },
            {
                title: 'Contact Us',
                component: ContactPage
            },
            {
                title: 'About Us',
                component: AboutPage
            },
            {
                title: 'Help',
                component: HelpPage
            },
            {
                title: 'Login',
                component: LoginPage
            }
        ];
        events.subscribe('user:created', function (user, time) {
            // user and time are the same arguments passed in `events.publish(user, time)`
            _this.userDetail = user;
            // console.log('Welcome', user, 'at', time);
        });
    }
    MyApp.prototype.ngOnInit = function () {
        var _this = this;
        this.storage.get('user').then(function (data) {
            _this.userDetail = JSON.parse(data);
        });
    };
    MyApp.prototype.openPage = function (page) {
        // close the menu when clicking a link from the menu
        this.menu.close();
        if (page.title == 'Logout') {
            // this.storage.clear();
            this.events.publish('user:logout', '', Date.now());
            this.userDetail = '';
        }
        // navigate to the new page if it is not the current page
        this.nav.setRoot(page.component, page.params);
    };
    MyApp.prototype.goLoginPage = function () {
        this.menu.close();
        this.nav.setRoot(LoginPage, { animate: false });
    };
    MyApp.prototype.goSignupPage = function () {
        this.menu.close();
        this.nav.setRoot(RegisterPage, { animate: false });
    };
    __decorate([
        ViewChild(Nav),
        __metadata("design:type", Nav)
    ], MyApp.prototype, "nav", void 0);
    MyApp = __decorate([
        Component({
            selector: 'app-root',
            templateUrl: 'app.html'
        }),
        __metadata("design:paramtypes", [Platform,
            StatusBar,
            SplashScreen,
            MenuController,
            App,
            Events,
            Storage])
    ], MyApp);
    return MyApp;
}());
export { MyApp };
//# sourceMappingURL=app.component.js.map