import { Component, ViewChild } from '@angular/core';
import { Storage } from '@ionic/storage';
import { Platform, MenuController, Nav, App, Events} from 'ionic-angular';
import { StatusBar } from '@ionic-native/status-bar';
import { SplashScreen } from '@ionic-native/splash-screen';
// import { SocialSharing } from '@ionic-native/social-sharing';

import { HomePage } from '../pages/home/home';
import { LoginPage } from '../pages/login/login';
import { RegisterPage } from '../pages/register/register';
import { AboutPage } from '../pages/about/about';
import { ContactPage } from '../pages/contact-us/contact-us';
// import { RecentViewPage } from '../pages/recent-view/recent-view';
import { WishListPage } from '../pages/wish-list/wish-list';
import { MyCartsPage } from '../pages/my-carts/my-carts';
import { AccountPage } from '../pages/account/account';
import { HelpPage } from '../pages/help/help';
import { AddAdressPage } from '../pages/add-address/add-address';
import { AddressListPage } from '../pages/address-list/address-list';
import { OrderSummeryPage } from '../pages/order-summery/order-summery';
import { MyOrdersPage } from '../pages/my-orders/my-orders';
// import { AuthPage } from '../pages/auth/auth';
// import { ProductListPage } from '../pages/product-list/product-list';


@Component({
  selector: 'app-root',
  templateUrl: 'app.html'
})
export class MyApp {
  @ViewChild(Nav) nav: Nav;
  userDetails:any;
  // make LearnFeedPage the root (or first) page
  rootPage: any = HomePage;

  pages: Array<{title: string, component: any}>;

  constructor(
    platform: Platform,
    statusBar: StatusBar,
    splashScreen: SplashScreen,
    public menu: MenuController,
    public app: App,
    public events: Events,
    public storage: Storage
  ) {
    platform.ready().then(() => {
      // Okay, so the platform is ready and our plugins are available.
      // Here you can do any higher level native things you might need.
      statusBar.styleDefault();
      splashScreen.hide();

    });
    this.storage.get('user').then((data) => {
      if(data != null){
        this.userDetails = JSON.parse(data);
      }
    });

    this.pages = [
      {
        title: 'Home',
        component: HomePage
      },
      // {
      //   title: 'Recently Viewed Item',
      //   component: RecentViewPage
      // },
      {
        title: 'My Cart',
        component: MyCartsPage
      },
      {
        title: 'My Orders',
        component: MyOrdersPage
      },
      {
        title: 'My Wish List',
        component: WishListPage
      },
      {
        title: 'My Account',
        component: AccountPage
      },
      {
        title: 'Contact Us',
        component: ContactPage
      },
      {
        title: 'About Us',
        component: AboutPage
      },
      {
        title: 'Help',
        component: HelpPage
      },
      {
        title: 'Login',
        component: LoginPage
      },
      {
        title: 'Log Out',
        component: LoginPage
      }
    ];

    events.subscribe('user:created', (user, time) => {
      // user and time are the same arguments passed in `events.publish(user, time)`
      this.userDetails = user;
      // console.log('Welcome', user, 'at', time);
    });
    
  }

  ngOnInit() {
    this.storage.get('user').then((data) => {
      this.userDetails = JSON.parse(data);
    });
  }

  openPage(page) {
    // close the menu when clicking a link from the menu
    this.menu.close();
    if(page.title == 'Logout'){
      // this.storage.clear();
      this.events.publish('user:logout', '', Date.now());
      this.userDetails = '';
    }
    // navigate to the new page if it is not the current page
    this.nav.setRoot(page.component, page.params);
  }

  logout(){
    this.menu.close();
    this.storage.clear();
    localStorage.clear();
    this.userDetails = "";
    this.nav.setRoot(HomePage, {animate: false});
  }
  goLoginPage(){
    this.menu.close();
    this.nav.setRoot(LoginPage, {animate: false});
  }
  goSignupPage(){
    this.menu.close();
    this.nav.setRoot(RegisterPage, {animate: false});
  }

}
