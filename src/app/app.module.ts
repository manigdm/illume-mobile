import { NgModule, ErrorHandler } from '@angular/core';
import { IonicStorageModule } from '@ionic/storage';
import { HttpModule } from '@angular/http';
import { IonicApp, IonicModule, IonicErrorHandler } from 'ionic-angular';
import { FormsModule } from '@angular/forms';

import { MyApp } from './app.component';

import { HomePage } from '../pages/home/home';
import { LoginPage } from '../pages/login/login';
import { RegisterPage } from '../pages/register/register';
import { ForgotPasswordPage } from '../pages/forgot-password/forgot-password';
import { AboutPage } from '../pages/about/about';
import { ContactPage } from '../pages/contact-us/contact-us';
import { RecentViewPage } from '../pages/recent-view/recent-view';
import { WishListPage } from '../pages/wish-list/wish-list';
import { MyCartsPage } from '../pages/my-carts/my-carts';
import { AccountPage } from '../pages/account/account';
import { HelpPage } from '../pages/help/help';
import { AuthPage } from '../pages/auth/auth';
import { ProductListPage } from '../pages/product-list/product-list';
import { ServicesModule } from '../services';
import { BrowserModule } from '@angular/platform-browser';
import { StatusBar } from '@ionic-native/status-bar';
import { SplashScreen } from '@ionic-native/splash-screen';
import { SocialSharing } from '@ionic-native/social-sharing';
import { ProductDetailPage } from "../pages/product-detail/product-detail";
import { DeliveryPage } from '../pages/delivery/delivery';
import { PaymentPage } from '../pages/payments/payments';
import { AddAdressPage } from '../pages/add-address/add-address';
import { AddressListPage } from '../pages/address-list/address-list';
import { OrderSummeryPage } from '../pages/order-summery/order-summery';
import { MyOrdersPage } from '../pages/my-orders/my-orders';

@NgModule({
  declarations: [
    MyApp,
    HomePage,
    LoginPage,
    RegisterPage,
    ForgotPasswordPage,
    AboutPage,
    ContactPage,
    RecentViewPage,
    WishListPage,
    MyCartsPage,
    AccountPage,
    HelpPage,
    AuthPage,
    ProductListPage,
    ProductDetailPage,
    DeliveryPage,
    PaymentPage,
    AddAdressPage,
    OrderSummeryPage,
    AddressListPage,
    MyOrdersPage
  ],
  imports: [
    BrowserModule,
    ServicesModule,
    HttpModule,
    FormsModule,
    IonicStorageModule.forRoot(),
    IonicModule.forRoot(MyApp)
  ],
  bootstrap: [IonicApp],
  entryComponents: [
    MyApp,
    HomePage,
    LoginPage,
    RegisterPage,
    ForgotPasswordPage,
    AboutPage,
    ContactPage,
    RecentViewPage,
    WishListPage,
    MyCartsPage,
    AccountPage,
    HelpPage,
    AuthPage,
    ProductListPage,
    ProductDetailPage,
    DeliveryPage,
    PaymentPage,
    AddAdressPage,
    OrderSummeryPage,
    AddressListPage,
    MyOrdersPage
  ],
  providers: [
    StatusBar,
    SplashScreen,
    SocialSharing,
    {provide: ErrorHandler, useClass: IonicErrorHandler}
  ]
})
export class AppModule {}
