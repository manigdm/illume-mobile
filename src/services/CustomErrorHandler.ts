import { Injectable, ErrorHandler } from '@angular/core';
// import { BaseService } from './BaseService';
import 'rxjs/add/observable/throw';

@Injectable()
export class CustomErrorHandler extends ErrorHandler {

    constructor() {
        super();
    }

    handleError(error) {
        super.handleError(error);
        this.handleHttpError(error);
    }

    handleHttpError(response: any) {
        //TODO: Error logging at server end using API endpoint
    }
}