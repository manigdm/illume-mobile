import { Injectable } from '@angular/core';
import { Observable } from 'rxjs/Observable';
import 'rxjs/add/operator/map';
import 'rxjs/add/operator/debounceTime';
import 'rxjs/add/operator/distinctUntilChanged';
import 'rxjs/add/operator/switchMap';
import 'rxjs/add/operator/do';
import { BaseService } from './BaseService';
import { apiUrl } from '../config';
// import { Subject } from 'rxjs/Subject';

@Injectable()
export class CoreService {
  // private loggedIn: boolean = false;
  apiUrl:any;
  title:any;
  constructor(private baseService: BaseService) {
  	this.apiUrl = apiUrl;
  }
  
  updateTile(title){
  	this.title = title;
  }

  getDeliveryDetails(id){
    let postData = {};
        postData['delivery_id'] = id;
    let url = this.apiUrl + 'checkout';
    return this.baseService.post(url, postData);
  }

  placeOrders(orderId, type){
    let postData = {};
        postData['payment_type'] = type;
        postData['order_id'] = orderId;
    let url = this.apiUrl + 'placeorder';
    return this.baseService.post(url, postData);
  }
  getMyOrders(){
    let url = this.apiUrl + 'myorders';
    return this.baseService.get(url);
  }
}
