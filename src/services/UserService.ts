import { Injectable } from '@angular/core';
import { Observable } from 'rxjs/Observable';
import { BaseService } from './BaseService';
// import { Subject } from 'rxjs/Subject';

@Injectable()
export class UserService {
  // private loggedIn: boolean = false;
  apiUrl:any;
  title:any;
  constructor(private baseService: BaseService) {
  	this.apiUrl = 'http://13.232.152.142/eschool/api/';
  }

  userLogin(postData): Observable<any> {
    let url = this.apiUrl + 'elogin';
    return this.baseService.post(url, postData);
  }

  userRegister(postData){
    let url = this.apiUrl + 'eregister';
    return this.baseService.post(url, postData);
  }

  saveUserAddress(postData){
    let url = this.apiUrl + 'addaddress';
    return this.baseService.post(url, postData);
  }

  getUserAddress(){
    let url = this.apiUrl + 'billingaddress';
    return this.baseService.get(url);
  }

}
