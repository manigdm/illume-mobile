import { Injectable } from '@angular/core';
import { Observable } from 'rxjs/Observable';
import { BaseService } from './BaseService';
// import { Subject } from 'rxjs/Subject';
import { apiUrl } from '../config';

@Injectable()
export class HomeService {
  // private loggedIn: boolean = false;
  title:any;
  constructor(private baseService: BaseService) {

  }

  getBanner() {
    let url = apiUrl + 'banner';

    return new Promise((resolve, reject) => {
        this.baseService.get(url).subscribe(res => {
            resolve(res);
        }, err => {
            reject(err);
        })
    })
  }

  getCategory(){
    let url = apiUrl + 'category';
    return new Promise((resolve, reject) => {
        this.baseService.get(url).subscribe(res => {
            resolve(res);
        }, err => {
            reject(err);
        })
    });
  }

  getFeatured(){
    let url = apiUrl + 'featured';
    return new Promise((resolve, reject) => {
        this.baseService.get(url).subscribe(res => {
            resolve(res);
        }, err => {
            reject(err);
        })
    });
  }

  getHomeData() {
      return Promise.all([this.getBanner(), this.getCategory(), this.getFeatured()]);
  }


  // TODO: Move these things to separate service

  getProductDetail(prod_id): Observable<any> {
    let url = `${apiUrl}productdetails?prod_id=${prod_id}`;
    return this.baseService.get(url);
  }

  addToCart(prod): Observable<any> {
      let url = `${apiUrl}addcart`;
      return this.baseService.post(url, prod);
  }

  getCartData() {
      let url = `${apiUrl}viewcart`;
      return this.baseService.get(url);
  }

  removeCartItem(item) {
        let url = `${apiUrl}removecart`;
      return this.baseService.post(url, item);
  }

  getSubCatList(cat_id){
    let url = `${apiUrl}subcategory?cat_id=${cat_id}`;
    return this.baseService.get(url);
  }

  getSubProductList(cat_id,type){
    let url = `${apiUrl}products?cat_id=${cat_id}&type=${type}`;
    return this.baseService.get(url);
  }
}
