import { NgModule, ErrorHandler } from '@angular/core';
import { BaseService } from './BaseService';
import { CustomErrorHandler } from './CustomErrorHandler';
import { CoreService } from './CoreService';
import { UserService } from './UserService';
import { HomeService } from './HomeService';
// import { AsdService } from './Product1Service';



@NgModule({
    providers: [
        BaseService, 
        CoreService, 
        UserService, 
        HomeService, 
        // AsdService,
        { provide: ErrorHandler, useClass: CustomErrorHandler }
    ],
})

export class ServicesModule {}
