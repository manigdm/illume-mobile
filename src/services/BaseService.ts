import { Injectable } from '@angular/core';
import { Http, Response, Headers, RequestOptions } from '@angular/http';
import { Events} from 'ionic-angular';
import { Storage } from '@ionic/storage';

import { Observable } from 'rxjs/Observable';
import 'rxjs/add/operator/map';
import 'rxjs/add/operator/catch';
import 'rxjs/add/observable/throw';

/**
* Http Service wrapper for every API calls 
*/
@Injectable()
export class BaseService {
    public body;
    public headers;
    public options;
    public userDetail:any;
    constructor(private _http: Http, public storage: Storage, public events: Events) {
        
    }
    
    /**
     * Set header and options for every request
     */
    public prepareHeader() {
        this.getUserAuth();
        var headerData = this.getHeaders();
        this.headers = new Headers(headerData);
        this.options = new RequestOptions({ headers: this.headers});
    }

    /**
     * Set header for every request
     */
    public getHeaders() {
        let headerData = {}
            // headerData['Accept'] = '*/*';
        if(this.userDetail){
            headerData['Authorization'] = `Bearer ${this.userDetail.token}`;
        }
        headerData['Content-Type'] = 'application/json';
        return headerData;
    }

    public postFileHeaders(){
        let headerData = {}
        // if(this.user){
        //     headerData[COMMON_CONFIG.AUTHORIZATION_BEARER] = 'Bearer' + ' ' + this.user.token;
        // }
        // headerData['Content-Type'] = 'multipart/form-data';

        return headerData;
    }

    /**
     *  Set meta data for every post/put request
     */
    public setMeta(data: any) {
        if (this.validateDataForHTML(data)) {
            throw new Error('Input data contains html content : ' + data);
        }
        this.body = JSON.stringify(data);
        return this;
    }

    /**
     * Observable get request
     */
    public get(url: string): Observable<any> {
        this.prepareHeader();
        return this._http.get(url, this.options)
            .map(this.extractData, this)
            .catch(this.handleError);
    }

    /**
     * Observable post request
     */
    /**
     * Observable post request
     */
    public post(url: string, data?: any): Observable<any> {
        this.prepareHeader();
        // let head = new Headers({ 'Content-Type': 'application/json' });
        // let options = new RequestOptions({ headers: head });
        return this.setMeta(data)._http.post(url, this.body, this.options)
            .map(this.extractData, this)
            .catch(this.handleError);
    }

    // public filePost(url: string, data?: any): Observable<any> {
        
    //     return this.setMeta(data)._http.post(url, this.body, this.options)
    //         .map(this.extractData, this)
    //         .catch(this.handleError);
    // }

    public filePost(url: string, data?: any): Observable<any> {
        let headerData = this.postFileHeaders();
        this.headers = new Headers(headerData);
        this.options = new RequestOptions({ headers: this.headers});
        return this._http.post(url,data, this.options)
            .map(this.extractData, this)
            .catch(this.handleError);
    }

    /**
     * Observable put request
     */
    public put(url: string, data?: any): Observable<any> {
        return this._http.put(url,data)
            .map(this.extractData, this)
            .catch(this.handleError);
    }

    /**
     * Observable delete request
     */
    public delete(url: string, data?: any): Observable<any> {
        return this._http.delete(url,data)
            .map(this.extractData, this)
            .catch(this.handleError);
    }

    /**
     * Extract data from response
     */
    private extractData(res) {
        let body = res.json();
        return body || {};
    }

    /**
     * Exception handling
     */
    private handleError(error: Response) {
        return Observable.throw(error.json() || 'Server error');
    }

    /**
     * Check for html tags
     */
    private validateDataForHTML(data) {
        let HTML_REGEX = "<(\"[^\"]*\"|'[^']*'|[^'\">])*>";
        var regex = new RegExp(HTML_REGEX);
        return regex.test(data) ? true : false;
    }

    public saveUserAuth(response){
        localStorage.setItem('user', JSON.stringify(response));
    }

    public getUserAuth(){
        this.events.subscribe('user:created', (user, time) => {
          // user and time are the same arguments passed in `events.publish(user, time)`
          this.userDetail = user;
          // console.log('Welcome', user, 'at', time);
        });
        this.events.subscribe('user:logout', (user, time) => {
          // user and time are the same arguments passed in `events.publish(user, time)`
          this.userDetail = user;
          // console.log('Welcome', user, 'at', time);
        });
        this.storage.get('user').then((data) => {
            if(data !='null'){
                this.userDetail = JSON.parse(data);
            }else{
                this.userDetail = '';
            }
            
        });

        this.userDetail = JSON.parse(localStorage.getItem('user'));
    }


    public clearUserAuth(){
        localStorage.clear();
    }

}