import { Component } from '@angular/core';
import { Storage } from '@ionic/storage';
import { NavController, ToastController, LoadingController} from 'ionic-angular';
import { DeliveryPage } from '../delivery/delivery';
import { LoginPage } from '../login/login';
import { AddAdressPage } from '../add-address/add-address';
import { imageUrl } from '../../config';


// TODO: Remove Home Service
import { CoreService } from '../../services/CoreService';
/**
 * Generated class for the ChildrenListPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@Component({
  selector: 'page-my-orders',
  templateUrl: 'my-orders.html',
})
export class MyOrdersPage {
    imageUrl = imageUrl;
    userDetails:any;
    productDetails:any;
    errorMsg:any;
    loader:any;
  	constructor(
      public storage:Storage, 
      public toastController : ToastController, 
      public navController: NavController,
      public coreService: CoreService,
      public loadingController: LoadingController
      ) {
        this.storage.get('user').then((data) => {
          this.userDetails = JSON.parse(data);
        });
        this.getMyOrders(); 
  	}

  	ionViewDidLoad() {

  	}
    ionViewWillEnter() {

    }

    getMyOrders(){
      this.coreService.getMyOrders().subscribe(res => {
          if(res.status != 401){
            this.productDetails = res.viewproduct;
          }else{
            this.errorMsg = 'Session expired, please login again'
            this.presentToast();
            this.navController.push(LoginPage, {login:true}, {animate: false});
          }
        }, err => {
      })
    }

    presentToast() {
      let toast = this.toastController.create({
        message: this.errorMsg,
        duration: 3000,
        position: 'bottom'
      });
      toast.present();
    }
    
    presentLoading() {
      this.loader = this.loadingController.create({
          content: 'Please wait...'
      });
      this.loader.present();
    }
}
