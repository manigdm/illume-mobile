import { Component } from '@angular/core';
import { Storage } from '@ionic/storage';
import { NavController, NavParams} from 'ionic-angular';
import { imageUrl } from '../../config';
import { HomePage } from '../home/home';
/**
 * Generated class for the LoginPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@Component({
  selector: 'page-order-summery',
  templateUrl: 'order-summery.html',
})
export class OrderSummeryPage {
	summeryDetails:any = {};
	imageUrl = imageUrl;
  	constructor(public storage: Storage, public navParams: NavParams, public navController: NavController) {
  		this.summeryDetails['priceDetails'] = {};
  	}

  	ionViewDidLoad() {
    	// console.log('ionViewDidLoad LoginPage');
  	}
  	ionViewWillEnter() {
  		this.summeryDetails['productDetails'] = this.navParams.data.deliveryDetails.product_views;
  		this.summeryDetails['addressDetails'] = this.navParams.data.deliveryDetails.details;
  		this.summeryDetails['priceDetails']['totalamt'] = this.navParams.data.deliveryDetails.totalamt;
  	}

  	goHome(){
  		this.navController.setRoot(HomePage, {login:true}, {animate: false});
  	}
}
