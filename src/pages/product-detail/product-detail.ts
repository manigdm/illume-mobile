import { Component } from '@angular/core';
import { Storage } from '@ionic/storage';
import { NavController, NavParams, ToastController, LoadingController} from 'ionic-angular';
import { ProductListPage } from '../product-list/product-list';
import { MyCartsPage } from '../my-carts/my-carts';
import { LoginPage } from '../login/login';
import { imageUrl } from '../../config';
import { DeliveryPage } from '../delivery/delivery';
import { AddAdressPage } from '../add-address/add-address';
import { AddressListPage } from '../address-list/address-list';
// import { ProductService } from '../../services/ProductService';

// TODO: Remove this from here
import { HomeService } from '../../services/HomeService';
import { UserService } from '../../services/UserService';

/**
 * Generated class for the ForgotPasswordPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@Component({
  selector: 'page-product-detail',
  templateUrl: 'product-detail.html',
})
export class ProductDetailPage {

  imageUrl = imageUrl;
  productDetail:any;
  addCart:any = [];
  productSize:any;
  cartProducts:any = [];
  loader:any;
  errorMsg:any;
  productData: any;
  userDetails:any;
  address:any;
  constructor(
    public navController: NavController, 
    public navParams:NavParams,
    public storage:Storage, 
    public toastController: ToastController, 
    public loadingController: LoadingController,
    public userService: UserService,
    // public productService: ProductService,

    // TODO: Remove HomeService
    public homeService: HomeService,
    ) {
      this.storage.get('myCartData').then((data) => {
         if(data != null){
           this.addCart = data;
         }
      });
      this.storage.get('user').then((data) => {
          if(data !='null'){
              this.userDetails = JSON.parse(data);
          }else{
              this.userDetails = '';
          }
          
      });
  }

  ionViewDidLoad() {
  }

  ionViewWillEnter() {
    this.getAddressDetail();
    this.productData = this.navParams.data.productDetail;
    this.homeService.getProductDetail(this.productData.prod_id).subscribe(res => {
      this.productDetail = res;
    }, err => {
      console.log('errr===', err);
    })
  }

  getAddressDetail(){
    this.userService.getUserAddress().subscribe(res => {
        this.address = res.address;
      }, err => {
    })
  }

  changeSize(size){
    console.log(size)
  }

  addToCart(productDetail){
    // if(this.productSize){
    //   if(this.addCart != null){
    //     let idx = this.addCart.findIndex(obj => obj['prod_id'] === productDetail.prod_id);
    //     if(idx == -1){
    //         productDetail.size = this.productSize;
    //         this.addCart.push(productDetail);
    //         this.storage.set('myCartData', this.addCart);
    //         this.navController.push(MyCartsPage, {animate: false});
    //     }else{
    //       this.errorMsg = 'Already added in cart';
    //       this.presentToast();
    //     }
    //   }else{
    //     this.addCart.push(productDetail);
    //     this.storage.set('myCartData', this.addCart);
    //     this.navController.push(MyCartsPage, {animate: false});
    //   }
    // }else{
    //   this.errorMsg = 'Please select product size';
    //   this.presentToast();
    // }

    if(this.productSize){
      if(this.userDetails){
        // TODO: Replace Home Service
        const prod = {
          "prod_id": productDetail.prod_id,
          "quantity": 1,
          "prod_token": productDetail.prod_token,
          "attribute":[this.productSize]
        };

        this.homeService.addToCart(prod).subscribe(res => {
          if(res.status != 401){
            this.navController.push(MyCartsPage, {animate: false});
          }else{
            this.errorMsg = 'Session expired, please login again'
            this.presentToast();
            this.navController.push(LoginPage, {login: true}, {animate: false});
            this.storage.clear();
          }
          
        }, err => {
          console.log('err====', err);
        });
      }else{
        this.errorMsg = 'Please login';
        this.presentToast();
        this.navController.push(LoginPage, {login: true}, {animate: false});
      }
    }else{
      this.errorMsg = 'Please select product size';
      this.presentToast();
    }
  }

  goBack(){
    this.navController.setRoot(ProductListPage, {animate: false});
  }

  buyNow(productDetail){
    if(this.productSize){
     if(this.userDetails !=undefined && this.userDetails !=null && this.userDetails !=undefined && this.userDetails != ""){
       const prod = {
          "prod_id": productDetail.prod_id,
          "quantity": 1,
          "prod_token": productDetail.prod_token,
          "attribute":[productDetail.prod_attribute]
        };
        this.homeService.addToCart(prod).subscribe(res => {
            if(res.status != 401){
              
            }else{
              this.errorMsg = 'Session expired, please login again'
              this.presentToast();
              this.navController.push(LoginPage, {login: true}, {animate: false});
              this.storage.clear();
            }
            
          }, err => {
            console.log('err====', err);
          });

        if(this.address && this.address.length > 0){
          this.navController.push(AddressListPage, {animate: false});
        }else{
          this.navController.push(AddAdressPage, {animate: false});
        }
      }else{
        this.navController.push(LoginPage, {login:true}, {animate: false});
      }
    }else{
      this.errorMsg = 'Please select product size';
      this.presentToast();
    }
  }

  presentToast() {
    let toast = this.toastController.create({
      message: this.errorMsg,
      duration: 3000,
      position: 'bottom'
    });
    toast.present();
  }
  
  presentLoading() {
    this.loader = this.loadingController.create({
        content: 'Please wait...'
    });
    this.loader.present();
  }

}
