import { Component } from '@angular/core';
import { Storage } from '@ionic/storage';
import { NavController, NavParams, ToastController, LoadingController} from 'ionic-angular';
import { PaymentPage } from '../payments/payments';
import { CoreService } from '../../services/CoreService';
import { UserService } from '../../services/UserService';
import { LoginPage } from '../login/login';
import { DeliveryPage } from '../delivery/delivery';
import { AddAdressPage } from '../add-address/add-address';
/**
 * Generated class for the LoginPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@Component({
  selector: 'page-address-list',
  templateUrl: 'address-list.html',
})
export class AddressListPage {
  userDetails:any;
  addreses:any;
  errorMsg:any;
  loader:any;
  selectedAddress:any;
  constructor(public storage: Storage, public navParams: NavParams, public navController: NavController, public toastController: ToastController, public userService: UserService, public coreService: CoreService, public loadingController: LoadingController) {
    this.storage.get('user').then((data) => {
       if(data != null){
        this.userDetails = JSON.parse(data);
       }
    });
    this.getAddressDetail();
      // this.deliveryDetails = this.navParams.data.OrderDetails;
  }

  ionViewDidLoad() {
    	// console.log('ionViewDidLoad LoginPage');
  }
  ionViewWillEnter() {

  }

  getAddressDetail(){
    this.userService.getUserAddress().subscribe(res => {
        if(res.status != 401){
          this.addreses = res.address;
        }else{
          this.errorMsg = 'Session expired, please login again'
          this.presentToast();
          this.navController.push(LoginPage, {login: true}, {animate: false});
          this.storage.clear();
        }
        
      }, err => {
    })
  }

  presentToast() {
    let toast = this.toastController.create({
      message: this.errorMsg,
      duration: 3000,
      position: 'bottom'
    });
    toast.present();
  }
  
  presentLoading() {
    this.loader = this.loadingController.create({
        content: 'Please wait...'
    });
    this.loader.present();
  }

  addNewAddress(){
    this.navController.push(AddAdressPage, {animate: false});
  }
  checkout(){
    if(this.selectedAddress){
      this.coreService.getDeliveryDetails(this.selectedAddress.bs_id).subscribe(res => {
        if(res.status != 401){
          this.navController.push(DeliveryPage, {deliveryDetails:res}, {animate: false});
        }else{
          this.errorMsg = 'Session expired, please login again'
          this.presentToast();
          this.navController.push(LoginPage, {login:true}, {animate: false});
        }
      }, err => {
      })
    }else{
      this.errorMsg = 'Please select address';
      this.presentToast();
    }
  }

}
