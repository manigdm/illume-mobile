import { Component } from '@angular/core';
import { NavController} from 'ionic-angular';
import { LoginPage } from '../login/login';
/**
 * Generated class for the ChildrenListPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@Component({
  selector: 'page-forgot-password',
  templateUrl: 'forgot-password.html',
})
export class ForgotPasswordPage {
    user:any = {};
    loader:any;
    errorMsg:any;
  constructor(public navController : NavController) {
  }

  ionViewDidLoad() {

  }

  goBack(){
    this.navController.setRoot(LoginPage, {animate: false});
  }

  getPassword(){
  	
  }
}
