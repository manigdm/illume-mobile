import { Component, Input } from '@angular/core';
import { Storage } from '@ionic/storage';
import { NavController, NavParams, MenuController } from 'ionic-angular';
import { CoreService } from '../../../services/CoreService';


/**
 * Generated class for the LoginPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@Component({
  selector: 'page-header',
  templateUrl: 'header.html'
})
export class HeaderPage {
  @Input() title;
  userDetail:any;
  constructor(public navCtrl: NavController, public navParams: NavParams, public menuController :MenuController, public coreService: CoreService, public storage: Storage) {
    
  }
  ngOnInit(){

  }

  ionViewDidLoad() {
    console.log('inner');
  }

}
