import { Component } from '@angular/core';
import { Storage } from '@ionic/storage';
import { NavController, ToastController} from 'ionic-angular';
import { DeliveryPage } from '../delivery/delivery';
import { LoginPage } from '../login/login';
import { AddAdressPage } from '../add-address/add-address';
import { imageUrl } from '../../config';
import { AddressListPage } from '../address-list/address-list';


// TODO: Remove Home Service
import { HomeService } from '../../services/HomeService';
import { UserService } from '../../services/UserService';
/**
 * Generated class for the ChildrenListPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@Component({
  selector: 'page-my-carts',
  templateUrl: 'my-carts.html',
})
export class MyCartsPage {
  	myCarts:any = [];
    total:any = 0.0;
    showEmptyCartMessage:boolean = false;
    OrderDetails:any = {};
    userDetail:any;
    imageUrl =  imageUrl;
    address:any;
  	constructor(
      public storage:Storage, 
      public toastController : ToastController, 
      public navController: NavController,
      public homeService: HomeService,
      public userService: UserService
      ) {
        this.storage.get('user').then((data) => {
          this.userDetail = JSON.parse(data);
        });
        this.getAddressDetail();
  	}

  	ionViewDidLoad() {

  	}
    ionViewWillEnter() {
      // this.total = 0.0;
      // this.OrderDetails['total_amount'] = '';
      // this.OrderDetails['data'] = {};

      // this.storage.get('myCartData').then((data) => {
      //    if(data != null){
      //       this.myCarts = data;
      //       if(this.myCarts.length > 0){
      //         this.myCarts.forEach( (item, index)=> {
      //           this.total = parseFloat(this.total) + parseFloat(item.amount);
      //           item.variant_price = "";
      //         })

      //       }else{
      //         this.showEmptyCartMessage = true;
      //       }
      //    }
      // });
      this.getCartData();
    }

    getCartData() {
      this.homeService.getCartData().subscribe(data => {
        if(data.cart_views.length > 0){
          this.myCarts = data;
          this.total = data.totalamt;
        }else{
          this.showEmptyCartMessage = true;
        }
      })
    }

    getAddressDetail(){
      this.userService.getUserAddress().subscribe(res => {
          this.address = res.address;
        }, err => {
      })
    }
    // changeQty(item, i, change){
    //   let price;
    //   console.log('item here', item, change);
    //   if(change < 0 && item.quantity == 1){
    //     this.removeItem(item);
    //     return;
    //   }
    //   if(item.qty >= 0){
    //     item.qty = parseInt(item.qty) + change;
    //     price = item.qty * item.price;
    //     item.variant_price = price;
    //     item.amount = price;
    //     this.myCarts[i] = item;
    //     this.storage.set("myCartData", this.myCarts).then( ()=> {
    //       this.toastController.create({
    //         message: "Cart Updated.",
    //         duration: 2000,
    //         showCloseButton: true
    //       }).present();

    //     });
    //     this.total = 0.0;
    //     this.myCarts.forEach( (item, index)=> {
    //       this.total = parseFloat(this.total) + parseFloat(item.amount);
    //     })
    //   }
    //   this.addOrderDetails();
       
    // }

    // addOrderDetails(){
    //   this.OrderDetails['total_amount'] = this.total;
    //   this.OrderDetails['data'] = this.myCarts;
    // }

    checkDelivery(){
      if(this.userDetail !=undefined && this.userDetail !=null && this.userDetail !=undefined && this.userDetail != ""){
        if(this.address){
          this.navController.push(AddressListPage, {animate: false});
        }else{
          this.navController.push(AddAdressPage, {animate: false});
        }
      }else{
        this.navController.push(LoginPage, {login:true}, {animate: false});
      }
    }


    removeItem(item) {
      this.homeService.removeCartItem({ord_id: item.ord_id}).subscribe(res => {
        console.log('res====', res);
        this.getCartData();
      }, err => {
        console.log('err===', err);
      })
    }
}
