import { Component } from '@angular/core';
import { NavController, ToastController, LoadingController, Events, NavParams} from 'ionic-angular';
import { Storage } from '@ionic/storage';
import { HomePage } from '../home/home';
import { RegisterPage } from '../register/register';
import { ForgotPasswordPage } from '../forgot-password/forgot-password';
import { MyCartsPage } from '../my-carts/my-carts';
import { UserService } from '../../services/UserService';
/**
 * Generated class for the LoginPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@Component({
  selector: 'page-login',
  templateUrl: 'login.html',
})
export class LoginPage {
   user:any = {};
   loader:any;
   errorMsg:any;
   deliveryDetails:any;
  constructor(public navController : NavController, public toastController: ToastController, public loadingController: LoadingController, public navParams: NavParams, public storage: Storage, public events: Events, public userService: UserService) {
      this.user = { email: "", password: ""};
      // this.user = { email: "sd@ssswvssv.com", password: "123456"};
  }

  ionViewDidLoad() {
    // console.log('ionViewDidLoad LoginPage');
  }
  userLogin(loginForm){
      if(loginForm.valid){
        this.presentLoading();
        this.userService.userLogin(this.user).subscribe(response=>{
          this.loader.dismiss();
          this.errorMsg = '';
          this.storage.set('user', JSON.stringify(response));
          localStorage.setItem('user', JSON.stringify(response));
          this.events.publish('user:created', response, Date.now());
          // if(this.navParams.data.OrderDetails){
          //   this.navController.setRoot(MyCartsPage, {animate: false});
          // }else if(this.navParams.data.fromProductDetail){
          //   this.navController.setRoot(HomePage, {animate: false});
          // }
          if(this.navParams.data.login){
            this.navController.setRoot(MyCartsPage, {animate: false});
          }else{
            this.navController.setRoot(HomePage, {animate: false});
          }
        },
        error => {
          this.loader.dismiss();
          this.errorMsg = error.message;
          this.presentToast();
        });
      }
  }
  goBack(){
    this.navController.setRoot(HomePage, {animate: false});
  }
  register(){
    this.navController.setRoot(RegisterPage, {animate: false});
  }
  forgotPassword(){
    this.navController.setRoot(ForgotPasswordPage, {animate: false});
  }
  
  presentToast() {
    let toast = this.toastController.create({
      message: this.errorMsg,
      duration: 3000,
      position: 'bottom'
    });
    toast.present();
  }
  presentLoading() {
    this.loader = this.loadingController.create({
        content: 'Please wait...'
    });
    this.loader.present();
  }
}
