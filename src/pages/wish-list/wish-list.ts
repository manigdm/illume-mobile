import { Component } from '@angular/core';
import { Storage } from '@ionic/storage';
// import { NavController, NavParams, Slides, ToastController} from 'ionic-angular';
/**
 * Generated class for the LoginPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@Component({
  selector: 'page-wish-list',
  templateUrl: 'wish-list.html',
})
export class WishListPage {
   myWishLists:any = [];
  	constructor(public storage: Storage) {
  		this.storage.get('myWishData').then((data) => {
	     	if(data != null){
	        this.myWishLists = JSON.parse(data);
          console.log(this.myWishLists);
	     	}
  		});
  	}

  	ionViewDidLoad() {
    	// console.log('ionViewDidLoad LoginPage');
  	}
}
