import { Component } from '@angular/core';
import { Storage } from '@ionic/storage';
import { NavController, NavParams, ToastController, LoadingController} from 'ionic-angular';
import { PaymentPage } from '../payments/payments';
import { AddressListPage } from '../address-list/address-list';
import { CoreService } from '../../services/CoreService';
import { UserService } from '../../services/UserService';
import { LoginPage } from '../login/login';
import { OrderSummeryPage } from '../order-summery/order-summery';
/**
 * Generated class for the LoginPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@Component({
  selector: 'page-delivery',
  templateUrl: 'delivery.html',
})
export class DeliveryPage {
  myWishLists:any = [];
  deliveryDetails:any = {};
  deliveryCharge:number = 150;
  myCarts:any = [];
  total:any = 0.0;
  showEmptyCartMessage:boolean = false;
  OrderDetails:any = {};
  userDetails:any;
  paymentType:any;
  errorMsg:any;
  loader:any;
  constructor(public storage: Storage, public navParams: NavParams, public navController: NavController, public toastController: ToastController, public loadingController: LoadingController, public coreService: CoreService) {
     //  this.deliveryDetails['data'] = [];
     //  this.deliveryDetails['total_amount'] = "";
  	  // this.storage.get('myCartData').then((data) => {
     //     if(data != null){
     //        this.deliveryDetails['data'] = data;
     //        if(this.deliveryDetails['data'].length > 0){
     //          this.deliveryDetails['data'].forEach( (item, index)=> {
     //            this.total = parseFloat(this.total) + parseFloat(item.amount);
     //            item.variant_price = "";
     //          })
     //          this.deliveryDetails['total_amount'] = this.total;
     //          this.addOrderDetails();

     //        }else{
     //          this.showEmptyCartMessage = true;
     //        }
     //     }
     //  });
      this.storage.get('user').then((data) => {
         if(data != null){
          this.userDetails = JSON.parse(data);
         }
      });
      
  }

  ionViewDidLoad() {
    	// console.log('ionViewDidLoad LoginPage');
  }
  ionViewWillEnter() {
    this.deliveryDetails = this.navParams.data.deliveryDetails;
  }
  // changeQty(item, i, change){
  //     let price;
  //     if(change < 0 && item.qty == 1){
  //       return;
  //     }
  //     if(item.qty >= 0){
  //       item.qty = parseInt(item.qty) + change;
  //       price = item.qty * item.price;
  //       item.variant_price = price;
  //       item.amount = price;
  //       this.deliveryDetails['data'][i] = item;
  //       this.storage.set("myCartData", this.deliveryDetails['data']).then( ()=> {
  //         this.toastController.create({
  //           message: "Cart Updated.",
  //           duration: 2000,
  //           showCloseButton: true
  //         }).present();

  //       });
  //       this.total = 0.0;
  //       this.deliveryDetails['data'].forEach( (item, index)=> {
  //         this.total = parseFloat(this.total) + parseFloat(item.amount);
  //       });
  //       this.deliveryDetails['total_amount'] = this.total;
  //     }
  //     this.addOrderDetails();
  // }

  // addOrderDetails(){
  //   this.OrderDetails['total_amount'] = this.deliveryDetails['total_amount'] + this.deliveryCharge;
  //   this.OrderDetails['data'] = this.deliveryDetails['data'];
  // }

  changeAddress(){
    this.navController.push(AddressListPage, {animate: false});
  }
  checkout(){
    if(this.paymentType){
      if(this.paymentType == 'cod'){
        this.coreService.placeOrders(this.deliveryDetails.order_no, this.paymentType).subscribe(res => {
          if(res.status != 401){
            console.log("success->",res)
            this.navController.push(OrderSummeryPage, {deliveryDetails:res}, {animate: false});
          }else{
            this.errorMsg = 'Session expired, please login again'
            this.presentToast();
            this.navController.push(LoginPage, {login:true}, {animate: false});
          }
        }, err => {
        })
      }else if(this.paymentType =='payu'){
        console.log('payu')
      }
    }else{
      this.errorMsg = 'Please select the payment type'
      this.presentToast();
    }
  }

  presentToast() {
    let toast = this.toastController.create({
      message: this.errorMsg,
      duration: 3000,
      position: 'bottom'
    });
    toast.present();
  }
  
  presentLoading() {
    this.loader = this.loadingController.create({
        content: 'Please wait...'
    });
    this.loader.present();
  }
}
