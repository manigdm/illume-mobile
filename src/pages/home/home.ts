import { Component } from '@angular/core';
import { NavController} from 'ionic-angular';
import { ProductListPage } from '../product-list/product-list';
import { ProductDetailPage } from "../product-detail/product-detail";
import { MyCartsPage } from '../my-carts/my-carts';
import { WishListPage } from '../wish-list/wish-list';
import {HomeService} from '../../services/HomeService';
import { imageUrl } from '../../config'
/**
 * Generated class for the LoginPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@Component({
  selector: 'page-home',
  templateUrl: 'home.html'
})
export class HomePage {

  public dataLoaded = false;
	public banners:any = [];
  public categories:any = [];
  public products:any = [];
  public imageUrl = imageUrl;

    constructor(public navController : NavController, public homeService: HomeService) {
      this.homeService.getHomeData().then((data: any) => {
        this.banners = data[0];
        this.categories = data[1];
        // if(data[2]) {
          this.products = data[2].data || [];
        // }
      }, err => {
        console.log('errr===', err);
      })
    }

    ionViewDidLoad() {
      
    }

    productList(category){
    	this.navController.push(ProductListPage, {category: category},{animate: false});
    }

    productDetail(productDetail) {
      this.navController.push(ProductDetailPage, {productDetail: productDetail}, {animate: false});
    }
    myCart() {
      this.navController.push(MyCartsPage, {animate: false});
    }
    wishList(){
      this.navController.push(WishListPage, {animate: false});
    }
}
