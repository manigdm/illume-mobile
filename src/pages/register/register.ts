import { Component } from '@angular/core';
import { Storage } from '@ionic/storage';
import { NavController, ToastController, LoadingController, Events} from 'ionic-angular';
import { LoginPage } from '../login/login';
import { HomePage } from '../home/home';
import { UserService } from '../../services/UserService';
/**
 * Generated class for the LoginPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@Component({
  selector: 'page-register',
  templateUrl: 'register.html',
})
export class RegisterPage {
  user:any = {};
  loader:any;
  errorMsg:any;
  constructor(public navController : NavController, public toastController: ToastController, public loadingController: LoadingController, public storage: Storage, public events: Events, public userService : UserService) {

  }

  ionViewDidLoad() {
    // console.log('ionViewDidLoad LoginPage');
  }

  userRegister(registerForm){
    if(registerForm.valid && this.user.terms != false){
      this.presentLoading();
      let postData = {};
          postData['name'] = this.user.name;
          postData['email'] = this.user.email;
          postData['password'] = this.user.password;
          postData['password_confirmation'] = this.user.password;
          postData['usertype'] = this.user.user_type;
          postData['school'] = this.user.school;
      this.userService.userRegister(postData).subscribe(response=>{
        this.loader.dismiss();
        this.errorMsg = '';
        this.storage.set('user', JSON.stringify(response));
        this.events.publish('user:created', response, Date.now());
        this.navController.setRoot(HomePage, {animate: false});
      },
      error => {
        this.loader.dismiss();
        this.errorMsg = error.message;
        this.presentToast();
      });
    }
  }

  goBack(){
    this.navController.setRoot(LoginPage, {animate: false});
  }

  presentToast() {
    let toast = this.toastController.create({
      message: this.errorMsg,
      duration: 3000,
      position: 'bottom'
    });
    toast.present();
  }
  presentLoading() {
    this.loader = this.loadingController.create({
        content: 'Please wait...'
    });
    this.loader.present();
  }

}
