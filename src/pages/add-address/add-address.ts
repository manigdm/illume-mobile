import { Component } from '@angular/core';
import { Storage } from '@ionic/storage';
import { NavController, NavParams, ToastController, LoadingController} from 'ionic-angular';
import { DeliveryPage } from '../delivery/delivery';
import { UserService } from '../../services/UserService';
import { LoginPage } from '../login/login';
import { AddressListPage } from '../address-list/address-list';
/**
 * Generated class for the LoginPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@Component({
  selector: 'page-add-address',
  templateUrl: 'add-address.html',
})
export class AddAdressPage {
    userDetails:any = [];
    userAdderss:any = {};
    checked: boolean = false;
    errorMsg: any;
    loader:any;
  	constructor(public storage: Storage, public navParams: NavParams, public navController: NavController, public userService: UserService, public toastController: ToastController, public loadingController: LoadingController) {
  		this.storage.get('user').then((data) => {
	     	if(data != null){
	        this.userDetails = JSON.parse(data);
	     	}
  		});

      this.userAdderss.enable_ship = true;
  	}

  	ionViewDidLoad() {
    	// console.log('ionViewDidLoad LoginPage');
  	}

    ionViewWillEnter() {
    }

    userAddress(addAddressForm){
    this.userAdderss['payment_type']= null;
      if(!this.checked){
        this.enableShipAddress();
      }
      if(addAddressForm.valid){
        this.userService.saveUserAddress(this.userAdderss).subscribe(res => {
          if(res.status == 401){
            this.errorMsg = 'Session expired, please login again'
            this.presentToast();
            this.navController.push(LoginPage, {login:true}, {animate: false});
          }else{
            this.navController.push(AddressListPage, {animate: false});
          }
        }, err => {
        })
      }
    }
    enableShipAddress(){
       this.checked = true;
       if(this.userAdderss.enable_ship){
          this.userAdderss.ship_firstname = this.userAdderss.bill_firstname;
          this.userAdderss.ship_lastname = this.userAdderss.bill_lastname;
          this.userAdderss.ship_companyname = this.userAdderss.bill_companyname;
          this.userAdderss.ship_email = this.userAdderss.bill_email;
          this.userAdderss.ship_phone = this.userAdderss.bill_phone;
          this.userAdderss.ship_country = this.userAdderss.bill_country;
          this.userAdderss.ship_address = this.userAdderss.bill_address;
          this.userAdderss.ship_city = this.userAdderss.bill_city;
          this.userAdderss.ship_state = this.userAdderss.bill_state;
          this.userAdderss.ship_postcode = this.userAdderss.bill_postcode;
       }else{
          this.userAdderss.ship_firstname = '';
          this.userAdderss.ship_lastname = '';
          this.userAdderss.ship_companyname = '';
          this.userAdderss.ship_email = '';
          this.userAdderss.ship_phone = '';
          this.userAdderss.ship_country = '';
          this.userAdderss.ship_address = '';
          this.userAdderss.ship_city = '';
          this.userAdderss.ship_postcode = '';
          this.userAdderss.bill_state = '';
       }
    }

  presentToast() {
    let toast = this.toastController.create({
      message: this.errorMsg,
      duration: 3000,
      position: 'bottom'
    });
    toast.present();
  }
  
  presentLoading() {
    this.loader = this.loadingController.create({
        content: 'Please wait...'
    });
    this.loader.present();
  }
}
