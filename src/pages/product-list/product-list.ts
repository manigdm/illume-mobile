import {Component, ViewChild} from '@angular/core';
import { Storage } from '@ionic/storage';
import { NavController, NavParams, Slides} from 'ionic-angular';
import { HomePage } from '../home/home';
import { ProductDetailPage } from '../product-detail/product-detail';
import { HomeService } from '../../services/HomeService';
import { imageUrl } from '../../config'
/**
 * Generated class for the ChildrenListPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@Component({
  selector: 'page-product-list',
  templateUrl: 'product-list.html',
})
export class ProductListPage {
  @ViewChild('slider') slider: Slides;
  @ViewChild("segments") segments;
  page: any;
  SwipedTabsIndicator :any= null;
  tabs:any=[];
  products:any = [];
  addToWishList:any = [];
  categoryData:any;
  public imageUrl = imageUrl;

  constructor(public navController: NavController, public navParams: NavParams, public storage:Storage, public homeService: HomeService) {
      this.storage.get('myWishData').then((data) => {
        if(data != null){
          this.addToWishList = JSON.parse(data);
        }
      });
      this.categoryData = this.navParams.data.category;
    // this.homeService.getCategory().then((res: any) => {
    //     this.tabs = res;
    //     // }
    //     console.log('cat result===', res);
    //   }, err => {
    //     console.log('errr===', err);
    //   })
    
    // this.products = [{'id':'1','qty':'1','product_name':"shirt","price":"600","amount":"600","img":['assets/img/cat2.jpg','assets/img/cat2.jpg','assets/img/cat2.jpg','assets/img/cat2.jpg'], "like":'true', 'description':'Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.'},{'id':'2','qty':'1','product_name':"Pants","price":"600","amount":"600","img":['assets/img/fp5.jpg','assets/img/fp5.jpg','assets/img/fp5.jpg','assets/img/fp5.jpg'], "like":'false', 'description':'Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.'},{'id':'3','qty':'1','product_name':"Shoes","price":"600","amount":"600","img":['assets/img/fp1.jpg','assets/img/fp1.jpg','assets/img/fp1.jpg','assets/img/fp1.jpg'], "like":'false', 'description':'Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.'},{'id':'4','qty':'1','product_name':"Accessories","price":"600","amount":"600","img":['assets/img/fp4.jpg','assets/img/fp4.jpg','assets/img/fp4.jpg','assets/img/fp4.jpg'], "like":'false', 'description':'Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.'}];
  }

  // Initialize slider
  ionViewDidEnter(){
    //this.slideChanged();
    this.getsubList();
    
  }

  getsubList(){
    this.homeService.getSubCatList(this.categoryData.id).subscribe(res => {
      this.tabs = res;
      this.page = '0';
      if(res && res.length > 0){
        this.getProductLists(this.tabs[0].subid,'cat');
      }
    }, err => {
    })
  }

  // On segment click
  selectedTab(index, tab) {
    this.slider.slideTo(index);
    this.getProductLists(tab.subid,'subcat');
  }

  getProductLists(cat_id,type){
    this.homeService.getSubProductList(cat_id, type).subscribe(res => {
      this.products = res.data;
    }, err => {
    })
  }
  


  // On slide changed
  slideChanged() {
    let currentIndex = this.slider.getActiveIndex();
    this.page = currentIndex.toString();
    this.centerScroll();
    
  }

  // Center current scroll
  centerScroll(){
    if(!this.segments || !this.segments.nativeElement)
      return;

    let sizeLeft = this.sizeLeft();
    let sizeCurrent = this.segments.nativeElement.children[this.page].clientWidth;
    let result = sizeLeft - (window.innerWidth / 2) + (sizeCurrent/2) ;

    result = (result > 0) ? result : 0;
    this.smoothScrollTo(result);
  }

  // Get size start to current
  sizeLeft(){
    let size = 0;
    for(let i = 0; i < this.page; i++){
      size+= this.segments.nativeElement.children[i].clientWidth;
    }
    return size;
  }

  // Easing function
  easeInOutQuart(time, from, distance, duration) {
    if ((time /= duration / 2) < 1) return distance / 2 * time * time * time * time + from;
    return -distance / 2 * ((time -= 2) * time * time * time - 2) + from;
  }

  // Animate scroll
  smoothScrollTo(endX){
    let startTime = new Date().getTime();
    let startX = this.segments.nativeElement.scrollLeft;
    let distanceX = endX - startX;
    let duration = 400;

    let timer = setInterval(() => {
      var time = new Date().getTime() - startTime;
      var newX = this.easeInOutQuart(time, startX, distanceX, duration);
      if (time >= duration) {
        clearInterval(timer);
      }
      this.segments.nativeElement.scrollLeft = newX;
    }, 1000 / 60); // 60 fps
  }
  goHome(){
  	this.navController.setRoot(HomePage, {animate: false});
  }
  // goProductDetail(){
  //   this.navController.setRoot(ProductDetailPage, {animate: false});
  // }

  addWishList(productDetail){
    if(this.addToWishList != null){
      let idx = this.addToWishList.findIndex(obj => obj['id'] === productDetail.id);
      if(idx == -1){
        this.addToWishList.push(productDetail);
      }else{
        this.addToWishList.splice(idx, 1);
      }
    }else{
      this.addToWishList.push(productDetail);
    }
    this.storage.set('myWishData', JSON.stringify( this.addToWishList ));

  }

  productDetail(productDetail){
    this.navController.push(ProductDetailPage, {productDetail:productDetail}, {animate: false});
  }

}
