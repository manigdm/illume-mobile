import { Component } from '@angular/core';
import { Storage } from '@ionic/storage';
import { NavController, NavParams, ToastController, LoadingController} from 'ionic-angular';
// import { InAppBrowser } from '@ionic-native/in-app-browser';
// import { OrderSummeryPage } from '../order-summery/order-summery';
/**
 * Generated class for the LoginPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@Component({
  selector: 'page-payment',
  templateUrl: 'payments.html',
})
export class PaymentPage {
  deliveryDetails:any = [];
  deliveryCharge:number = 150;
  paymentType:any;
  errorMsg:any;
  loader:any;
	constructor(public storage: Storage, public navParams: NavParams, public toastController: ToastController, public loadingController: LoadingController, public navController: NavController) {
    this.deliveryDetails = this.navParams.data.DeliveryDetails;
	}

	ionViewDidLoad() {
  	// console.log('ionViewDidLoad LoginPage');
	}

  checkPayment(){
    if(this.paymentType && this.paymentType != undefined){
      this.errorMsg = "oops! please contact admin to make payment as of now"
      this.presentToast();
    }else{
      this.errorMsg = "Please select payment type"
      this.presentToast();
    }
  }
  // handlePayment(results) {
  //   if (results.result == 'success') {
  //   let browser = this.iab.create(results.redirect, '_blank', 'location=yes');
  //   browser.on('loadstart').subscribe(data => {
  //       if (data.url.indexOf('order-received') != -1 && data.url.indexOf('return=') == -1) {
  //         var str = data.url;
  //         var pos1 = str.lastIndexOf("/order-received/");
  //         var pos2 = str.lastIndexOf("/?key=wc_order");
  //         var pos3 = pos2 - (pos1 + 16);
  //         var order_id = str.substr(pos1 + 16, pos3);
  //         this.navController.push('OrderSummeryPage', order_id);
  //         browser.close();
  //       }
  //       else if (data.url.indexOf('cancel_order=true') != -1 || data.url.indexOf('cancelled=1') != -1 || data.url.indexOf('cancelled') != -1) {
  //         browser.close();
  //         console.log('Order Cancelled');
  //       }    
  //   });
  //   }
  //   else if (results.result == 'failure') {
  //     this.errorMsg = results.messages;
  //     this.presentToast();
  //   }
  // }

  presentToast() {
    let toast = this.toastController.create({
      message: this.errorMsg,
      duration: 3000,
      position: 'bottom'
    });
    toast.present();
  }
  presentLoading() {
    this.loader = this.loadingController.create({
        content: 'Please wait...'
    });
    this.loader.present();
  }
}
